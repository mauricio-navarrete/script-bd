CREATE DATABASE  IF NOT EXISTS `full-stack-ecommerce` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `full-stack-ecommerce`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: full-stack-ecommerce
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `unit_price` decimal(13,2) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  `units_in_stock` int DEFAULT NULL,
  `date_created` datetime(6) DEFAULT NULL,
  `last_updated` datetime(6) DEFAULT NULL,
  `category_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category` (`category_id`),
  CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `product_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `sku`, `name`, `description`, `unit_price`, `image_url`, `active`, `units_in_stock`, `date_created`, `last_updated`, `category_id`) VALUES (1,'CALZADO-NIKE-1000','Nike Air Force','El fulgor vive en Nike Air Force 1 ’07, el ícono del básquetbol que le da un toque fresco',89990.00,'assets/img/1.png',_binary '',100,'2021-05-28 17:34:57.000000',NULL,1);
INSERT INTO `product` (`id`, `sku`, `name`, `description`, `unit_price`, `image_url`, `active`, `units_in_stock`, `date_created`, `last_updated`, `category_id`) VALUES (2,'CALZADO-NIKE-1001','Nike Blazer Low X','Elogiado en las calles por su simplicidad y comodidad clásicas',89990.00,'assets/img/2.png',_binary '',100,'2021-05-28 17:35:02.000000',NULL,1);
INSERT INTO `product` (`id`, `sku`, `name`, `description`, `unit_price`, `image_url`, `active`, `units_in_stock`, `date_created`, `last_updated`, `category_id`) VALUES (3,'CALZADO-NIKE-1002','Nike Air Max95 Essential','El Nike Air Max 95 Essential toma las pautas de diseño del cuerpo humano y las lleva a tus pies',144990.00,'assets/img/3.png',_binary '',100,'2021-05-28 17:35:15.000000',NULL,1);
INSERT INTO `product` (`id`, `sku`, `name`, `description`, `unit_price`, `image_url`, `active`, `units_in_stock`, `date_created`, `last_updated`, `category_id`) VALUES (4,'CALZADO-NIKE-1003','Jordan Delta 2','El Jordan Delta 2 ofrece una versión fresca e intrépida con las características que quieres: durabilidad, comodidad y una actitud Jordan por donde lo veas',114990.00,'assets/img/4.png',_binary '',100,'2021-05-28 17:35:19.000000',NULL,1);
INSERT INTO `product` (`id`, `sku`, `name`, `description`, `unit_price`, `image_url`, `active`, `units_in_stock`, `date_created`, `last_updated`, `category_id`) VALUES (5,'CALZADO-NIKE-1004','Nike Blazer MID','En la década del 70, Nike era el nuevo del barrio',92990.00,'assets/img/5.png',_binary '',100,'2021-05-28 17:35:25.000000',NULL,1);
INSERT INTO `product` (`id`, `sku`, `name`, `description`, `unit_price`, `image_url`, `active`, `units_in_stock`, `date_created`, `last_updated`, `category_id`) VALUES (6,'ACCESORIO-CREP-1000','Crep Protect 200ml Can','Spray protector que repele agua y polvo. Spray superhidrofóbico de 200 ml que crea un recubrimiento invisible que repele los líquidos y evita las manchas.  Para usar en calzado de gamuza, nubuck y lona. ',15990.00,'assets/img/crep.png',_binary '',100,'2021-05-29 18:30:00.000000',NULL,2);
INSERT INTO `product` (`id`, `sku`, `name`, `description`, `unit_price`, `image_url`, `active`, `units_in_stock`, `date_created`, `last_updated`, `category_id`) VALUES (7,'ACCESORIO-CREP-1001','Crep Protect - Cure Ultimate Cleaning Kit','Crep Improved Cleaning Solution 100 ml., Que se puede utilizar para más de 50 lavados de calzado. Elaborado a base de extractos de coco, jojoba y agua. Perfecto para limpiar tus zapatos.',19990.00,'assets/img/sprite.png',_binary '',100,'2021-05-29 18:30:00.000000',NULL,2);
INSERT INTO `product` (`id`, `sku`, `name`, `description`, `unit_price`, `image_url`, `active`, `units_in_stock`, `date_created`, `last_updated`, `category_id`) VALUES (8,'ACCESORIO-CREP-1002','ACCESORIOS CREP PROTEC PILLS','Absorbe la humedad y elimina activamente los malos olores. Reemplaza los malos olores con una fragancia fresca.',12990.00,'assets/img/pill.png',_binary '',100,'2021-05-29 18:30:00.000000',NULL,2);
INSERT INTO `product` (`id`, `sku`, `name`, `description`, `unit_price`, `image_url`, `active`, `units_in_stock`, `date_created`, `last_updated`, `category_id`) VALUES (9,'ACCESORIO-CREP-1003','CORDONES CREP PROTEC (FLAT) NEGRO','Tecnología Crep Protect infundida en las fibras de los cordones para garantizar la máxima protección. Repele el líquido y previene de las manchas con una protección que durará hasta 10 lavados.',7990.00,'assets/img/cordon.png',_binary '',100,'2021-05-29 18:30:00.000000',NULL,2);
INSERT INTO `product` (`id`, `sku`, `name`, `description`, `unit_price`, `image_url`, `active`, `units_in_stock`, `date_created`, `last_updated`, `category_id`) VALUES (10,'ACCESORIO-CREP-1004','Crep Protect','Kit de Limpieza para tus Zapatos The Ultimate Sneaker Care Kit Crep Protect',9990.00,'assets/img/kit.png',_binary '',100,'2021-05-29 18:30:00.000000',NULL,2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
